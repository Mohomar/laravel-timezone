<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Mohoma Timezones </title>
        <style>
            html, body {
                height: 100%;
            }
 
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
 
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
 
            .content {
                text-align: center;
                display: inline-block;
            }
 
            .title {
                font-size: 96px;
            }
        </style>
        <script></script>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">{{ $time }}</div>
            </div>
        </div>
    </body>
</html>