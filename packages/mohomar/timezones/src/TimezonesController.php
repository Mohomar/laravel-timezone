<?php

namespace Mohomar\Timezones;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class TimezonesController extends Controller
{
	public function index($timezone = NULL){
		//print Carbon::now($timezone)->toDateTimeString();
            $time = ($timezone) 
                    ? Carbon::now(str_replace('-','/',$timezone))
                    : Carbon::now();
            return view("timezones::time", ['time'=>$time->toDateTimeString()]);
	}
}